
public class TestaListaEstaticaDesordenada {

	public static void main(String[] args) {
		ListaEstaticaDesordenada lista = new ListaEstaticaDesordenada();
		lista.inicializar(5);
		System.out.println(lista.tamanhoAtual()); // 0
		
		lista.inserir(10);
		lista.removerUltimoElemento();
		System.out.println(lista.tamanhoAtual());
		
		lista.removerUltimoElemento();
		System.out.println("Tamanho A: " + lista.tamanhoAtual());
		
		lista.inserir(10);
		lista.inserir(20);
		lista.inserir(30);
		
		System.out.println("Tamanho B: " + lista.tamanhoAtual());;//3
		
		lista.removerPrimeiroElemento();
		
		System.out.println("Tamanho C: " + lista.tamanhoAtual());//2
		
		lista.imprimir();//20,30
		
		lista.removerPrimeiroElemento();
		lista.removerPrimeiroElemento();
		System.out.println();
		System.out.println();
		System.out.println("Tamanho D: " + lista.tamanhoAtual());//0
		
		lista.removerPrimeiroElemento();
		System.out.println();
		System.out.println("Tamanho E: " + lista.tamanhoAtual());//0
		
		
		
//		System.out.println(lista.tamanhoAtual()); // 3
//
//		lista.imprimir(); //10,20,30,
//		System.out.println();
//		
//		lista.inserir(40);
//		lista.inserir(50);
//		
//		lista.imprimir(); //10,20,30,40,50,
//		
//		lista.inserir(60);
		
		
		
		
		
		
		
		
		
		
		
		
		
	}

}
