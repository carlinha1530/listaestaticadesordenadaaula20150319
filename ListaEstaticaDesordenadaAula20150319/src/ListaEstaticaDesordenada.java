
public class ListaEstaticaDesordenada {

	int qtdElementos = 0;
	int[] valores;
	
	void inicializar(int numElementos) {
		if (numElementos <= 0) {
			numElementos = 10;
		}
		valores = new int[numElementos];
	}
	
	void inserir(int novoValor) {
		if (qtdElementos >= valores.length) {
			System.err.println("limite excedido");
			return;
		}
		valores[qtdElementos] = novoValor;
		++qtdElementos;
	}
	
	int tamanhoAtual() {
		return qtdElementos;
	}
	
	void imprimir() {
		for (int i = 0; i < qtdElementos; i++) {
			System.out.print(valores[i] + ", ");
		}
	}
	
	void removerUltimoElemento() {
		if (qtdElementos > 0) {
			--qtdElementos;
		}
	}
	
	void removerPrimeiroElemento() {
		for (int i = 0; i < qtdElementos; i++) {
			valores[i] = valores[i+1];
		}
		--qtdElementos;
	}
	
	int removerDaPosicao(int posicao) {
		return 10;
	}
	
	
	
	
	
	
}
